﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    using System.ServiceModel;

    using SuperService;
    using SuperService.Data;

    public class People : ClientBase<IPeopleService>, IPeopleService
    {
        public IEnumerable<Person> All()
        {
            return base.Channel.All();
        }

        public bool Add(Person person)
        {
            return base.Channel.Add(person);
        }

        public bool Remove(Guid id)
        {
            return base.Channel.Remove(id);
        }

        public bool Remove(Person person)
        {
            return base.Channel.Remove(person);
        }
    }
}
