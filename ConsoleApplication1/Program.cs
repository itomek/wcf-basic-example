﻿namespace ConsoleApplication1
{
    using System;
    using System.Linq;

    using SuperService.Data;

    class Program
    {
        static void Main(string[] args)
        {
            var peopleService = new People();

            Console.WriteLine("[ --- WCF Service Exmplae --- ]");

            string command = Console.ReadLine();

            while (!command.Equals("exit"))
            {
                if (command.Equals("list"))
                {
                    var people = peopleService.All();

                    if (!people.Any())
                    {
                        continue;
                    }

                    people.ToList().ForEach(person => Console.WriteLine(person.ToString()));

                    Console.WriteLine();
                }
                else if (command.StartsWith("add"))
                {
                    var cmds = command.Split(' ');

                    int age;
                    int.TryParse(cmds[3], out age);

                    var result = peopleService.Add(new Person
                    {
                        FirstName = cmds[1],
                        LastName = cmds[2],
                        Age = age
                    });

                    Console.WriteLine(result ? "Success" : "Fail");
                }
                else if (command.StartsWith("remove"))
                {
                    var cmds = command.Split(' ');

                    bool result = default(bool);

                    // must be an id
                    if (cmds.Length == 2)
                    {
                        Guid id;
                        Guid.TryParse(cmds[1], out id);

                        result = peopleService.Remove(id);
                    }
                    // must be a name
                    else
                    {
                        result = peopleService.Remove(new Person { FirstName = cmds[1], LastName = cmds[2] });
                    }

                    Console.WriteLine(result ? "Success" : "Fail");
                }

                Console.WriteLine("Ready.......");
                command = Console.ReadLine();
            }
        }
    }
}
