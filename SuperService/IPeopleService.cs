﻿namespace SuperService
{
    using System;
    using System.Collections.Generic;
    using System.ServiceModel;

    using SuperService.Data;

    [ServiceContract]
    public interface IPeopleService
    {
        [OperationContract]
        IEnumerable<Person> All();

        [OperationContract]
        bool Add(Person person);

        [OperationContract]
        bool Remove(Guid id);

        [OperationContract(Name = "RemoveByName")]
        bool Remove(Person person);
    }
}