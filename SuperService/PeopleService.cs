﻿namespace SuperService
{
    using System;
    using System.Collections.Generic;

    using SuperService.Data;

    public class PeopleService : IPeopleService
    {
        private readonly DataBase db = DataBase.Instance;
        
        public IEnumerable<Person> All()
        {
            return this.db.People;
        }

        public bool Add(Person person)
        {
            return this.db.Add(person);
        }

        public bool Remove(Guid id)
        {
            return this.db.Remove(id);
        }

        public bool Remove(Person person)
        {
            return this.db.Remove(person);
        }
    }
}