﻿namespace SuperService
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using SuperService.Data;

    public class DataBase
    {
        public List<Person> People { get; private set; }

        public static readonly DataBase instance = new DataBase();

        public static DataBase Instance
        {
            get
            {
                return instance;
            }
        }

        private DataBase()
        {
            this.People = new List<Person>
            {
                new Person { Id = Guid.NewGuid(), FirstName = "Xavier", LastName = "Smith", Age = 23 },
                new Person { Id = Guid.NewGuid(), FirstName = "Donald", LastName = "Johnson", Age = 57 },
                new Person { Id = Guid.NewGuid(), FirstName = "Dominik", LastName = "Yewedlewski", Age = 19 },
                new Person { Id = Guid.NewGuid(), FirstName = "Cap", LastName = "Podrap", Age = 40 },
                new Person { Id = Guid.NewGuid(), FirstName = "John", LastName = "Werthington", Age = 33 }
            };
        }

        public bool Add(Person person)
        {
            var initialCount = this.People.Count;

            person.Id = Guid.NewGuid();

            this.People.Add(person);

            return this.People.Count > initialCount;
        }

        public bool Remove(Guid id)
        {
            var initialCount = this.People.Count;

            var found = this.People.Single(p => p.Id == id);

            this.People.Remove(found);

            return this.People.Count < initialCount;
        }

        public bool Remove(Person person)
        {
            var initialCount = this.People.Count;

            var result = this.People.Find(p => p.FirstName.Equals(person.FirstName) && p.LastName.Equals(p.LastName));

            if (result != null)
            {
                this.People.Remove(result);
            }

            return this.People.Count < initialCount;
        }
    }
}