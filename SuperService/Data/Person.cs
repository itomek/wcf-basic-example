﻿namespace SuperService.Data
{
    using System;

    public class Person : IPerson
    {
        public Guid Id { get; set; }
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int Age { get; set; }

        public override string ToString()
        {
            return string.Format("{0} {1} [{2}]", this.FirstName, this.LastName, this.Id);
        }
    }
}