﻿namespace SuperService.Data
{
    using System;

    public interface IPerson
    {
        Guid Id { get; set; }
        
        string FirstName { get; set; }

        string LastName { get; set; }

        int Age { get; set; }
    }
}
